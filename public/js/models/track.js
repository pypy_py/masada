"use strict";

import { getYoutubeThumbnailPath } from "../modules/utils.js";

class TrackModel {
    _id;
    img_src;
    img_src_wide;
    comments = [];
    upload_time;
    duration;
    name;
    description;;
    favorites = [];
    poster;

    constructor(path, name, description, upload_time, poster, img_src, img_src_wide, duration) {
        this.path = path;
        this.name = name;
        this.description = description;
        this.upload_time = upload_time;
        this.poster = poster;
        this.img_src = img_src;
        this.img_src_wide = img_src_wide;
        this.duration = duration;
    }

    setImagePath(path) {
        this.img_src = getYoutubeThumbnailPath(path);
    }
}

class PlaylistModel {
    _id;
    name;
    created_at;
    user;
    tracks;

    constructor(name, created_at, user, tracks) {
        this.name = name ?? "";
        this.created_at = created_at;
        this.user = user;
        this.tracks = tracks ?? [];
    }
}

export { TrackModel, PlaylistModel };
