/* global client_info, language */

import * as HttpHandler from "./modules/http.js";
import { i18n } from "./modules/locale.js";

// Init Bootstrap Toast
let toastElList = [].slice.call(document.querySelectorAll(".toast"));
let toastList = toastElList.map(toastEl => {
    return new bootstrap.Toast(toastEl, {
        animation: false,
        delay: 2000
    });
});

let app = Vue.createApp({
    data() {
        return {
            preferences: client_info.preferences ?? {},
            lang: language,
        };
    },

    methods: {
        setLanguage(lang) {
            HttpHandler.postAsync(HttpHandler.SET_LANG_URL, {
                language: lang
            }, (res) => {
                setTimeout(() => {
                    window.location.reload(true);
                }, 500);
            });
        },
        // updatePreferences() {
        //     HttpHandler.putAsync(HttpHandler.USER_UPDATE_URL + client_info.username, {
        //         preferences: this.preferences
        //     }, (res) => {
        //         res.json().then((data) => {
        //             if (data.error) {
        //                 showToastMessage(this.$t("message.toast_preferences_save_fail"));
        //                 return;
        //             }

        //             showToastMessage(this.$t("message.toast_preferences_save"));
        //             setTimeout(() => {
        //                 window.location.reload(true);
        //             }, 500);
        //         });
        //     });
        // }
    }
});

app.use(i18n);

app.mount("#app");

function showToastMessage(message) {
    toastList[0].show();
    toastElList[0].querySelector(".toast-body > p").innerText = message;
}
