import * as HttpHandler from "./modules/http.js";
import trackItemComponent from "./components/track_item.js";
import { commentItemComponent } from "./components/comment_item.js";

// Init Bootstrap Toast
let toastElList = [].slice.call(document.querySelectorAll(".toast"));
let toastList = toastElList.map(toastEl => {
    return new bootstrap.Toast(toastEl, {
        animation: false,
        delay: 2000
    });
});

Vue.createApp({
    components: {
        "comment-item": commentItemComponent,
        "track-item": trackItemComponent
    },
    data() {
        return {
            comments_list: [],
            is_empty: false
        };
    },
    mounted() {
        HttpHandler.getAllPostsWithComments().then(result => {
            if (result.error) {
                showToastMessage("Could not get comments...");
            } else {
                HttpHandler.getListenersInfo().then(listener_result => {
                    result.data.forEach(val => {
                        if (Object.keys(listener_result).includes(val._id)) {
                            val.listeners = listener_result[val._id].listeners;
                        } else {
                            val.listeners = [];
                        }
                    });

                    // Sort the posts chronologically
                    // Really should have set a separate comments collection huh...
                    let sorted_posts = [];
                    result.data.forEach(post => {
                        sorted_posts.push({
                            most_recent_comment: post.comments[post.comments.length - 1],
                            post: post
                        });
                    });

                    let i, j;
                    for (i = 0; i < sorted_posts.length - 1; i++) {
                        for (j = 0; j < sorted_posts.length - i - 1; j++) {
                            if (sorted_posts[j].most_recent_comment.timestamp > sorted_posts[j + 1].most_recent_comment.timestamp) {
                                let temp = sorted_posts[j];
                                sorted_posts[j] = sorted_posts[j + 1];
                                sorted_posts[j + 1] = temp;
                            }
                        }
                    }

                    let final = [];
                    sorted_posts.forEach(val => {
                        final.push(val.post);
                    });
                    final.reverse();

                    this.comments_list = final;

                    if (this.comments_list.length === 0) this.is_empty = true;
                });
            }
        });
    },
}).mount("#app");


function showToastMessage(message) {
    toastList[0].show();
    toastElList[0].querySelector(".toast-body > p").innerText = message;
}
