const paginationItemComponent = {
    props: ["text", "href", "onclick", "isActive", "isDisabled"],
    template: `
        <li class="page-item" :class="{active: isActive, disabled: isDisabled}" v-on:click="onclick($event)">
            <a class="page-link" :href="href">
                {{ text }}
            </a>
        </li>
    `,
};

export default paginationItemComponent;
