import { MicroUser } from "../models/user.js";

const trackItemComponent = {
    props: {
        title: String,
        href: String,
        img_src: String,
        listeners_count: {
            type: Number,
            required: false,
            default: 0,
            validator(value) {
                return Number.isSafeInteger(value);
            }
        },
        favorites_count: {
            type: Number,
            required: false,
            default: 0,
            validator(value) {
                return Number.isSafeInteger(value);
            }
        },
        comments_count: {
            type: Number,
            required: false,
            default: 0,
            validator(value) {
                return Number.isSafeInteger(value);
            }
        },
        poster: {
            type: MicroUser,
            default: new MicroUser(),
        },
        upload_time: {
            type: Date,
            default: null
        },
        show_avatar: {
            type: Boolean,
            default: true
        }
    },
    computed: {
        getTooltip() {
            let text = this.title;
            let is_not_empty = this.favorites_count || this.listeners_count || this.comments_count;
            if (is_not_empty){
                let listener_text = this.listeners_count > 0 ? this.listeners_count + `${this.listeners_count > 1 ? " listeners" : " listener"}` + " " : "";
                let favorites_text = this.favorites_count > 0 ? this.favorites_count + `${this.favorites_count > 1 ? " favorites" : " favorite"}` + " " : "";
                let comments_text = this.comments_count > 0 ? this.comments_count + `${this.comments_count > 1 ? " comments" : " comment"}` + " " : "";
				if ((comments_text !== "" || favorites_text !== "") && listener_text !== "") listener_text = listener_text.concat(", ");
				if (comments_text !== "" && favorites_text !== "") favorites_text = favorites_text.concat(", ");
                text = text.concat("\n\nThis track has ", listener_text, favorites_text, comments_text);
            }
            return text;
        },

        getRelativeTime() {
            if (moment) {
                return moment(this.upload_time).fromNow();
            }

            return this.upload_time;
        }
    },
    template: `
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xxl-2 text-start track-item">
            <a v-bind:title="getTooltip" :href="href">
                <div class="position-relative track-item-container w-100 ratio ratio-4x3"  :style="{ 'background-image':  'url(' + img_src + ')'   } " style="background-size: cover; background-repeat: no-repeat;">
                    <div style="display:flex; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px;" class="p-sm-3 p-2 w-100 h-100 flex-column justify-content-between align-items-start position-absolute bottom-0 start-0">
                        <div class="d-flex flex-row align-items-center w-100">
                            <div class="flex-row align-items-center card-background badge mw-100 flex-wrap" v-show="favorites_count > 0 || comments_count > 0 || listeners_count > 0" style="display: flex">
                                <div class="me-2 flex-row align-items-center" v-show="listeners_count !== undefined && listeners_count > 0" style="display: flex">
                                    <svg v-if="listeners_count > 1"  xmlns="http://www.w3.org/2000/svg" class=" -tabler -tabler-users" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="9" cy="7" r="4" />
                                        <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75" />
                                        <path d="M21 21v-2a4 4 0 0 0 -3 -3.85" />
                                    </svg>

                                    <svg v-else xmlns="http://www.w3.org/2000/svg" class="-tabler -tabler-user" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="7" r="4" />
                                        <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                                    </svg>

                                    <p class="ms-1">{{ listeners_count }}</p>
                                </div>

                                <div v-show="favorites_count > 0" class="flex-row align-items-center me-2" style="display:flex;">
                                    <svg xmlns="http://www.w3.org/2000/svg" class=" -tabler -tabler-heart" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" fill="#fff" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <path d="M19.5 13.572l-7.5 7.428l-7.5 -7.428m0 0a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572" />
                                    </svg>

                                    <p class="ms-1">{{ favorites_count }}</p>
                                </div>

                                <div v-show="comments_count > 0"  class="me-2 flex-row align-items-center" style="display: flex;">
                                    <svg xmlns="http://www.w3.org/2000/svg" class=" -tabler -tabler-message-2" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <path d="M12 20l-3 -3h-2a3 3 0 0 1 -3 -3v-6a3 3 0 0 1 3 -3h10a3 3 0 0 1 3 3v6a3 3 0 0 1 -3 3h-2l-3 3" />
                                        <line x1="8" y1="9" x2="16" y2="9" />
                                        <line x1="8" y1="13" x2="14" y2="13" />
                                    </svg>

                                    <p class="ms-1">{{ comments_count }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

            <div class="d-flex flex-row mt-2 align-items-start">
                <a class="flex-shrink-0 me-2" v-show="show_avatar && poster.username && poster.username !==''" :href="'/user/' + poster.username">
                    <img class="user-avatar rounded-circle img-fluid" style="margin-top: 3px;" :src="poster.avatar_url" loading="lazy" />
                </a>
                <div>
                    <a :href="href">
                        <p class="track-item-title fw-bold" :title="title"> {{ title }} </p>
                    </a>
                    <div class="track-item-description d-flex flex-row flex-wrap text-secondary">
                        <a class="text-secondary" :href="'/user/' + poster.username">
                            <p> {{ poster.username }} </p>
                        </a>
                        <span class="mx-1">&middot;</span>
                        <p :title="upload_time"> {{ getRelativeTime }} </p>
                    </div>
                </div>
            </div>
        </div>
    `
};

export default trackItemComponent;
