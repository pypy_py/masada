"use strict";

import trackItemComponent from "./components/track_item.js";
import progressBarComponent from "./components/progress_bar.js";

import { getAllSongsInfo } from "./modules/song.js";

import * as HttpHandler from "./modules/http.js";
import { LOADING_ICON_PATH } from "./modules/utils.js";

import { TrackModel } from "./models/track.js";
import { i18n } from "./modules/locale.js";

import { getYoutubeTrackById } from "./modules/utils.js";

// Init Bootstrap Toast
let toastElList = [].slice.call(document.querySelectorAll(".toast"));
let toastList = toastElList.map(toastEl => {
    return new bootstrap.Toast(toastEl, {
        animation: false,
        delay: 2000
    });
});

let app = Vue.createApp({
    components: {
        "track-item": trackItemComponent,
        "progress-bar": progressBarComponent
    },
    data() {
        return {
            all_songs_info: null,
            upload_input: null,
            get_video_info_task: null,
            upload_info: new TrackModel(),
            can_upload: false,
            is_uploading: false,
            track: new TrackModel(),
        };
    },

    methods: {
        parseYoutubeURL(url) {
            //eslint-disable-next-line
            let regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
            let match = url.match(regExp);
            return (match && match[1].length == 11) ? match[1] : false;
        },

        parseUploadInput() {
            let url_match = this.parseYoutubeURL(this.upload_input);

            if (url_match) {
                if (this.get_video_info_task) clearTimeout(this.get_video_info_task);

                this.track.is_visible = true;
                this.track.img_src = LOADING_ICON_PATH;
                this.track.name = this.$t("message.loading");
                this.track.duration = null;

                this.get_video_info_task = setTimeout(() => {
                    getYoutubeTrackById(url_match, (res) => {
                        try {
                            if (!res.error && res.data.items.length > 0) {
                                this.track.setImagePath(url_match);
                                this.track.name = res.data.items[0].snippet.title;
                                this.track.duration = res.data.items[0].contentDetails.duration;

                                this.can_upload = true;

                                this.upload_info.path = url_match;
                                this.upload_info.name = this.track.name;
                                this.upload_info.duration = this.track.duration;

                                // Get the highest resolution thumbnail available
                                let thumbnail_info = res.data.items[0].snippet.thumbnails;

                                // Wide
                                if (thumbnail_info.maxres) {
                                    this.upload_info.img_src_wide = thumbnail_info.maxres.url;
                                } else if (thumbnail_info.standard) {
                                    this.upload_info.img_src_wide = thumbnail_info.standard.url;
                                } else if (thumbnail_info.high) {
                                    this.upload_info.img_src_wide = thumbnail_info.high.url;
                                } else if (thumbnail_info.medium) {
                                    this.upload_info.img_src_wide = thumbnail_info.medium.url;
                                } else if (thumbnail_info.default) {
                                    this.upload_info.img_src_wide = thumbnail_info.default.url;
                                }

                                // Normal
                                if (thumbnail_info.high) {
                                    this.upload_info.img_src = thumbnail_info.high.url;
                                } else if (thumbnail_info.medium) {
                                    this.upload_info.img_src = thumbnail_info.medium.url;
                                } else if (thumbnail_info.default) {
                                    this.upload_info.img_src = thumbnail_info.default.url;
                                }
                            } else {
                                this.track.img_src = LOADING_ICON_PATH;
                                this.track.name = this.$t("message.upload_invalid_url");

                                this.can_upload = false;

                                this.upload_info = new TrackModel();
                                showToastMessage(res.error.message);
                            }
                        } catch (err) {
                            this.track.img_src = LOADING_ICON_PATH;
                            this.track.name = this.$t("message.toast_something_wrong");

                            this.can_upload = false;

                            this.upload_info = new TrackModel();
                        }
                    });
                }, 1000);
            } else {
                this.track.is_visible = true;
                this.track.img_src = LOADING_ICON_PATH;
                this.track.name = this.$t("message.upload_invalid_url");


                this.upload_info = new TrackModel();
                this.can_upload = false;
            }
        },
        upload() {
            if (!this.can_upload || this.is_uploading) {
                showToastMessage(this.$t("message.upload_invalid_data"));
                return;
            }

            this.is_uploading = true;
            this.$refs.progressbar.setStartedStatus();

            HttpHandler.postAsync(HttpHandler.UPLOAD_TRACK_URL, this.upload_info, (res) => {
                if (res.status === 200) {
                    res.json().then((data) => {
                        if (!data.error) {
                            showToastMessage(this.$t("message.upload_success"));
                            this.resetInputFields();
                        } else {
                            showToastMessage(this.$t("message.upload_fail"));
                            this.resetInputFields();
                        }
                    });
                } else if (res.status === 500) {
                    showToastMessage(this.$t("message.toast_something_wrong"));
                }

                this.is_uploading = false;
                this.can_upload = false;
                this.$refs.progressbar.setFinishedStatus();
            });
        },

        resetInputFields() {
            this.upload_input = "";
            this.upload_info = new TrackModel();
            this.$refs["upload-box"].focus();

            this.track.is_visible = false;
            this.track.img_src = LOADING_ICON_PATH;
            this.track.name = this.$t("message.loading");
        }
    },

    mounted() {
        i18n.global.locale = HttpHandler.setLangFromCookie();
        getAllSongsInfo().then((data) => {
            this.all_songs_info = data;

            initSearchBox(this.all_songs_info);
        }).catch(() => {
            showToastMessage(this.$t("message.toast_something_wrong"));
        });
    }
});
app.use(i18n);
app = app.mount("#app");

function initSearchBox(data) {
    new autoComplete({
        data: {
            src: data,
            keys: ["search_keywords"],
            cache: true,
        },
        selector: "#search-box",
        resultItem: {
            element: (item, data) => {
                item.innerHTML = data.value.name;
                item.innerText = data.value.name;
            },
        },
        resultsList: {
            maxResults: 20,
        },
    });

    document.getElementById("search-box").addEventListener("selection", (event) => {
        window.location.replace("/?track_id=" + event.detail.selection.value._id);
    });
}

function showToastMessage(message) {
    toastList[0].show();
    toastElList[0].querySelector(".toast-body > p").innerText = message;
}
