import { CommentModel } from "../models/comment.js";
import * as HttpHandler from "./http.js";

export function addComment(content, post_id) {
    return new Promise((resolve, reject) => {
        HttpHandler.postAsync(HttpHandler.POST_COMMENT_URL, {
            content: content,
            post_id: post_id
        }, (res) => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        });
    });
}

export async function createCommentItem(username, content, timestamp) {
    try {
        let user_info = (await HttpHandler.getUserInfoByUsername(username)).info;

        return new CommentModel(user_info, content, timestamp);
    } catch(e) {
        return;
    }
}
