export const BASE_URL = ""; // Test URL
export const RANDOM_BG_URL = BASE_URL + "/image/random";
export const GET_ALL_BG_INFO_URL = BASE_URL + "/image/ordered-all";

export const GET_BG_PICKER_TAGS_URL = BASE_URL + "/image/tags";
export const GET_BG_PICKER_TAG_ART_URL = BASE_URL + "/image/tags/data";

export const INCREMENT_LISTENER_COUNT_URL = BASE_URL + "/listener/increment-count";
export const DECREMENT_LISTENER_COUNT_URL = BASE_URL + "/listener/decrement-count";
export const LISTENER_INFO_URL = BASE_URL + "/listener/all";
export const HOMEPAGE_LISTENER_INFO_URL = BASE_URL + "/listener/homepage";
export const RANDOM_TRACK_URL = BASE_URL + "/track/random";
export const RELATED_TRACK_URL = BASE_URL + "/track/related";
export const RELATED_PLAYLISTS_URL = BASE_URL + "/track/playlist/related";
export const FEATURED_POST_URL = BASE_URL + "/track/featured";
export const GET_YOUTUBE_TRACK_BY_ID_URL = BASE_URL + "/track/youtube-id";
export const GET_ALL_TRACK_INFO_URL = BASE_URL + "/track/all";
export const GET_PAGED_TRACK_INFO_URL = BASE_URL + "/track/page";
export const GET_TRACK_BY_ID_URL = BASE_URL + "/track/info";
export const UPLOAD_TRACK_URL = BASE_URL + "/track/upload";
export const EDIT_TRACK_URL = BASE_URL + "/track/update";
export const DELETE_TRACK_URL = BASE_URL + "/track";

export const CREATE_PLAYLIST_URL = BASE_URL + "/track/playlist";
export const GET_PLAYLIST_URL = BASE_URL + "/track/playlist";
export const DELETE_PLAYLIST_URL = BASE_URL + "/track/playlist";
export const GET_USER_PLAYLISTS_URL = BASE_URL + "/track/playlist";
export const GET_PLAYLIST_DETAIL_URL = BASE_URL + "/track/playlist/detail";
export const GET_IS_TRACK_IN_PLAYLIST_URL = BASE_URL + "/track/playlist/check-track";
export const GET_PLAYLIST_TRACK_IDS_URL = BASE_URL + "/track/playlist/track-ids";
export const ADD_TRACK_TO_PLAYLIST_URL = BASE_URL + "/track/playlist/add";
export const REMOVE_TRACK_FROM_PLAYLIST_URL = BASE_URL + "/track/playlist/remove";

export const USER_INFO_URL = BASE_URL + "/user/all";
export const USER_LOGIN_URL = BASE_URL + "/user/login";
export const USER_REGISTER_URL = BASE_URL + "/user/register";
export const USER_UPDATE_URL = BASE_URL + "/user/"; // append the user-to-update's username for a full URL
export const USER_UPDATE_PASSWORD_URL = BASE_URL + "/user/password/"; // append the user-to-update's username for a full URL
export const GET_USER_INFO_URL = BASE_URL + "/user/info/"; // append the user's username for a full URL
export const SET_LANG_URL = BASE_URL + "/lang";

export const GET_ALL_COMMENTS_URL = BASE_URL + "/track/comment";
export const GET_HOMEPAGE_COMMENTS_URL = BASE_URL + "/track/comment-homepage";
export const POST_COMMENT_URL = BASE_URL + "/track/comment";

export const FETCH_SIZE = 42;

export function getAsync(theUrl) {
    return new Promise((resolve) => {
        let xmlHttp = new XMLHttpRequest();

        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState === 4) {
                resolve(xmlHttp);
            }
        };

        xmlHttp.open("GET", theUrl, true); // true for asynchronous

        xmlHttp.send(null);
    });
}

export function postAsync(url, data, callback) {
    fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Host": "rpgradio.me",
        },
    }).then(res => {
        callback(res);
    });
}

export function putAsync(url, data, callback) {
    fetch(url, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Host": "rpgradio.me",
        },
    }).then(res => {
        callback(res);
    });
}

export function deleteAsync(url, data, callback) {
    fetch(url, {
        method: "DELETE",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Host": "rpgradio.me",
        },
    }).then(res => {
        callback(res);
    });
}


export async function getBGArt(tag, offset = 0, size = FETCH_SIZE) {
	const data = await fetch(GET_BG_PICKER_TAG_ART_URL + "?" + new URLSearchParams({
		tag,
		offset,
		size
	}), {
		method: "GET",
	});
	return await data.json();
}
export async function getBGTags() {
	const data = await fetch(GET_BG_PICKER_TAGS_URL, {
		method: "GET",
	});
	return await data.json();
}

export async function getRandomTrack() {
	const data = await fetch(RANDOM_TRACK_URL, {
		method: "GET",
	});
	return await data.json();
}

export async function getRelatedPlaylists(track_id, offset = 0, size = FETCH_SIZE) {
	const data = await fetch(RELATED_PLAYLISTS_URL + "?" + new URLSearchParams({
		track_id,
		offset,
		size
	}), {
		method: "GET",
	});
	return await data.json();
}

export async function getRelatedTracks() {
	const data = await fetch(RELATED_TRACK_URL, {
		method: "GET",
	});
	return await data.json();
}

export async function getTrackById(id) {
	const data =  await fetch(GET_TRACK_BY_ID_URL +  `?id=${id}`, {
		method: "GET",
	});
	return await data.json();
}

export function getUserInfoByUsername(username) {
    return new Promise((resolve, reject) => {
        fetch(GET_USER_INFO_URL + username, {
            method: "GET",
        }).then(res => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        }).catch(err => {
            reject(err);
        });
    });
}

export function updateUserInfo(username, data, callback) {
    putAsync(USER_UPDATE_URL + username, data, (res) => {
        res.json().then((data) => {
            callback(data);
        });
    });
}

export function updateTrack(data, callback) {
    putAsync(EDIT_TRACK_URL, data, (res) => {
        res.json().then((data) => {
            callback(data);
        });
    });
}

export function getAllBGInfo(callback) {
    fetch(GET_ALL_BG_INFO_URL, {
        method: "GET",
    }).then(res => {
        res.json().then(data => {
            callback(data);
        });
    });
}

export function getHomepageComments() {
	return new Promise((resolve, reject) => {
		fetch(GET_HOMEPAGE_COMMENTS_URL, {
			method: "GET",
		}).then(res => {
			res.json().then(data => {
				resolve(data);
			}).catch(err => {
				reject(err);
			});
		}).catch(err => {
			reject(err);
		});
	});
}

export function getAllPostsWithComments() {
    return new Promise((resolve, reject) => {
        fetch(GET_ALL_COMMENTS_URL, {
            method: "GET",
        }).then(res => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        }).catch(err => {
            reject(err);
        });
    });
}

export function getFeaturedPost() {
    return new Promise((resolve, reject) => {
        fetch(FEATURED_POST_URL, {
            method: "GET",
        }).then(res => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        }).catch(err => {
            reject(err);
        });
    });
}


export function getListenersInfo() {
    return new Promise((resolve, reject) => {
        fetch(LISTENER_INFO_URL, {
            method: "GET",
        }).then(res => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        }).catch(err => {
            reject(err);
        });
    });
}

export function getPlaylist(playlist_id) {
    return new Promise((resolve, reject) => {
        fetch(GET_PLAYLIST_URL + "?id=" + playlist_id, {
            method: "GET",
        }).then(res => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        }).catch(err => {
            reject(err);
        });
    });
}

export function getRandomBG(limited = true, is_mobile = false) {
    return new Promise((resolve, reject) => {
        fetch(RANDOM_BG_URL + "?type=" + (limited ? "limited" : "unlimited") + "&client_type=" + (is_mobile ? "mobile" : "desktop"), {
            method: "GET",
        }).then(res => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        }).catch(err => {
            reject(err);
        });
    });
}


export async function getPlaylistTrackIds(id) {
	return new Promise((resolve, reject) => {
		fetch(GET_PLAYLIST_TRACK_IDS_URL + "?" + new URLSearchParams({
			id
		}), {
			method: "GET",
		}).then(res => {
			res.json().then(data => {
				resolve(data);
			}).catch(err => {
				reject(err);
			});
		}).catch(err => {
			reject(err);
		});
	});
}

export async function getIsTrackInPlaylist(track_id, playlist_id) {
	return new Promise((resolve, reject) => {
		fetch(GET_IS_TRACK_IN_PLAYLIST_URL + "?" + new URLSearchParams({
			track_id,
			playlist_id
		}), {
			method: "GET",
		}).then(res => {
			res.json().then(data => {
				resolve(data);
			}).catch(err => {
				reject(err);
			});
		}).catch(err => {
			reject(err);
		});
	});
}


export async function getPlaylistDetail(id, offset = 0, size = FETCH_SIZE) {
	return new Promise((resolve, reject) => {
		fetch(GET_PLAYLIST_DETAIL_URL + "?" + new URLSearchParams({
			id,
			offset,
			size
		}), {
			method: "GET",
		}).then(res => {
			res.json().then(data => {
				resolve(data);
			}).catch(err => {
				reject(err);
			});
		}).catch(err => {
			reject(err);
		});
	});
}



export async function getUserPlaylists() {
    return new Promise((resolve, reject) => {
        fetch(GET_USER_PLAYLISTS_URL, {
            method: "GET",
        }).then(res => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        }).catch(err => {
            reject(err);
        });
    });
}

export async function createPlaylist(name, tracks = []) {
    return new Promise((resolve, reject) => {
        postAsync(CREATE_PLAYLIST_URL, {
            name: name.trim(),
            tracks: tracks
        }, (res) => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        });
    });
}

export function addTrackToPlaylist(track_id, playlist_id) {
    return new Promise((resolve, reject) => {
        postAsync(ADD_TRACK_TO_PLAYLIST_URL, {
            track_id: track_id,
            playlist_id: playlist_id
        }, (res) => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        });
    });
}

export function removeTrackFromPlaylist(track_id, playlist_id) {
    return new Promise((resolve, reject) => {
        postAsync(REMOVE_TRACK_FROM_PLAYLIST_URL, {
            track_id: track_id,
            playlist_id: playlist_id
        }, (res) => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        });
    });
}

export function deleteTrack(track_id) {
    return new Promise((resolve, reject) => {
        deleteAsync(DELETE_TRACK_URL, {
            track_id: track_id
        }, (res) => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        });
    });
}

export function deletePlaylist(playlist_id) {
    return new Promise((resolve, reject) => {
        deleteAsync(DELETE_PLAYLIST_URL, {
            id: playlist_id
        }, (res) => {
            res.json().then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        });
    });
}

export function setLangFromCookie() {
    const lang = _getCookie("language");
    if (typeof moment !== "undefined") {
        moment.locale(lang);
    }
    return lang;
}

function _getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

setLangFromCookie();
