import * as HttpHandler from "./modules/http.js";
import { i18n } from "./modules/locale.js";

// Init Bootstrap Toast
let toastElList = [].slice.call(document.querySelectorAll(".toast"));
let toastList = toastElList.map(toastEl => {
    return new bootstrap.Toast(toastEl, {
        animation: false,
        delay: 2000
    });
});

let app = Vue.createApp({
    data() {
        return {
            username: "",
            password: "",
            email: "",
        };
    },
    mounted() {
        i18n.global.locale = HttpHandler.setLangFromCookie();
    },
    methods: {
        register() {
            HttpHandler.postAsync(HttpHandler.USER_REGISTER_URL, {
                username: this.username,
                password: this.password,
                email: this.email ?? ""
            }, (res) => {
                res.json().then((result) => {
                    if (result.error) {
                        showToastMessage(result.error.message);
                    } else {
                        showToastMessage(this.$t("message.register_success"));
                        setTimeout(() => {
                            window.location.replace("/");
                        }, 1000);
                    }
                });
            });
        }
    }
});
app.use(i18n);
app = app.mount("#app");

function showToastMessage(message) {
    toastList[0].show();
    toastElList[0].querySelector(".toast-body > p").innerText = message;
}
