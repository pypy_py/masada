<img src="/doc/icon.png" width=200 height=200/>

# Masada
A platform to collect music and art.

Still in alpha!

## Features: 
- Repost videos from YouTube as embeds
- Collect and view art from a [Szurubooru](https://github.com/rr-/szurubooru) instance
- Chat features
- Post comments
- Post favoriting
- Minimal UI. Use background art on all your pages or just a simple solid color background.
- English & Japanese localization

## Installation
```
git clone https://gitlab.com/neonflowr/masada.git
```

## Deployment

This assumes you already have Docker and Docker Compose installed.

**1. Configure the application**
- Make sure you're in the base directory of the application.
```
cd masada
```
- Copy the example `.env` to the base directory.

```
cp doc/example.env .env
```

- Edit the `.env` file to your usage.
    - Change your MongoDB configuration for example if you're using a managed hosting service or deploying without Docker, else leave it unchanged.
    - Add in your YouTube API key.
    - Add in your Szurubooru credentials.

```
edit .env
```

**2. Run with Docker Compose**
```
docker-compose up -d
```
**3. Miscellaneous**
- View logs
```
docker-compose logs -f 
```

- Stop
```
docker-compose down
```

## Screenshots
Homepage

![homepage](/doc/screenshots/1.png)

Tracks view

![track](/doc/screenshots/2.png)

Player view

![player](/doc/screenshots/3.png)

Background view

![background](/doc/screenshots/4.png)

Background picker

![picker](/doc/screenshots/5.png)

## License
[GPLv3](/LICENSE)
