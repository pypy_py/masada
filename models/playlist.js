class PlaylistModel {
    _id;
    name;
    created_at;
    user;
    tracks;

    constructor(name, created_at, user, tracks) {
        this.name = name;
        this.created_at = created_at;
        this.user = user;
        this.tracks = tracks;
    }
}

export { PlaylistModel };
